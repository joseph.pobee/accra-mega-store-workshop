package io.turntabl.accramegastore;

import io.turntabl.accramegastore.exception.UnknownProductException;
import io.turntabl.accramegastore.services.product.Product;
import io.turntabl.accramegastore.services.product.ProductService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;


public class ProductServiceTest {

    @Test
    @Disabled
    public void doesNotAddDuplicateProductId() {
        ProductService productService = new ProductService("products/duplicate_products.csv");
        assertEquals(1, productService.getProducts().size());
    }

    @Test
    public void productsCanBeAddedSuccessfully() {
        ProductService productService = new ProductService("products/valid_products.csv");
        assertEquals(2, productService.getProducts().size());
        assertThat(productService.getProduct("001")).hasFieldOrPropertyWithValue("name", "Frozen Green Peas");
        assertThat(productService.getProduct("002")).hasFieldOrPropertyWithValue("name", "Tea - Natural Care");
    }

    @Test
    public void skipProductsWithInvalidQuantity() {
        ProductService productService = new ProductService("products/valid_products.csv");
        assertThrows(UnknownProductException.class, ()-> {
           productService.getProduct("003");
        });
    }

    @Test
    public void noNegativeQuantities() {
        ProductService productService = new ProductService("products/valid_products.csv");
        List<Product> products = productService.getProducts();
        for(Product p : products) {
            assertTrue(p.getStockLevel() >= 0);
        }
    }
}
