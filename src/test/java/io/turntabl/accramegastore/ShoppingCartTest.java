package io.turntabl.accramegastore;

import io.turntabl.accramegastore.exception.UnavailableProductException;
import io.turntabl.accramegastore.services.ShoppingCart;
import io.turntabl.accramegastore.services.product.Product;
import io.turntabl.accramegastore.services.product.ProductService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;


public class ShoppingCartTest {

    @Test
    public void addsProductToCartSuccessfully() {
        ProductService productService = new ProductService("products/valid_products.csv");
        ShoppingCart cart = new ShoppingCart(productService);
        assertEquals(0, cart.getItems().size());
        cart.addProduct("001", 1);
        assertEquals(1, cart.getItems().size());
    }

    @Test
    public void decrementsStockWhenAddedToCart() {
        ProductService productService = new ProductService("products/valid_products.csv");
        int expectedQuantity = productService.getProduct("001").getStockLevel() - 1;
        ShoppingCart cart = new ShoppingCart(productService);
        cart.addProduct("001", 1);
        assertEquals(expectedQuantity, productService.getProduct("001").getStockLevel());
    }

    @Test
    public void doesNotAddOutOfStockProductToCart() {

    }

    @Test
    public void removeProductFromCartSuccessfully() {
        ProductService prodService = Mockito.mock(ProductService.class);
        Product product1 = new Product("001", "Frozen Peas", "Fresho", 270, 10, "Frozen Vegetables");
        Mockito.when(prodService.getProduct("001")).thenReturn(product1);

        //given a shopping cart with a product added
        ShoppingCart cart = new ShoppingCart(prodService);
        cart.addProduct("001", 1);

        //when a cart item is removed
        cart.removeProduct("001", 1);

        //cart should not have product
        assertEquals(0, cart.getItems().size());
    }

    @Test
    public void decrementsQuantityFromCartSuccessfully() {
        ProductService prodService = Mockito.mock(ProductService.class);
        Product product1 = new Product("001", "Frozen Peas", "Fresho", 270, 10, "Frozen Vegetables");
        Mockito.when(prodService.getProduct("001")).thenReturn(product1);

        //given a shopping cart with multiple products added
        ShoppingCart cart = new ShoppingCart(prodService);
        cart.addProduct("001", 5);

        //when a cart item is removed
        cart.removeProduct("001", 1);

        //quantity should be decremented
        assertEquals(4, cart.getItems().get(0).getQuantity());
    }

    @Test
    public void throwsExceptionWhenProductToBeRemovedDoesNotExist() {
        ProductService prodService = Mockito.mock(ProductService.class);

        //given a cart with no products
        ShoppingCart cart = new ShoppingCart(prodService);

        //when a non-exist product is removed, it throws an exception
        assertThrows(UnavailableProductException.class, () -> {
            cart.removeProduct("001", 1);
        });
    }

    @Test
    public void makesCallToUpdateProductStockLevelWhenItemRemovedFromCart() {
        ProductService prodService = Mockito.mock(ProductService.class);
        Product product1 = new Product("001", "Frozen Peas", "Fresho", 270, 10, "Frozen Vegetables");
        Mockito.when(prodService.getProduct("001")).thenReturn(product1);

        //given a shopping cart with multiple products added
        ShoppingCart cart = new ShoppingCart(prodService);
        cart.addProduct("001", 5);
        Mockito.verify(prodService).updateProduct("001", 5);


        //when a cart item is removed
        cart.removeProduct("001", 1);
//        Mockito.verify(prodService).updateProduct("001", 6);

    }
}
