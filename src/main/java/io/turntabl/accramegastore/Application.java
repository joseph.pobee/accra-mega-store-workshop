package io.turntabl.accramegastore;

import io.turntabl.accramegastore.exception.UnavailableProductException;
import io.turntabl.accramegastore.exception.UnknownCommandException;
import io.turntabl.accramegastore.exception.UnknownProductException;
import io.turntabl.accramegastore.services.Item;
import io.turntabl.accramegastore.services.product.Product;
import io.turntabl.accramegastore.services.discount.DiscountService;
import io.turntabl.accramegastore.services.recommendation.ComplementaryService;
import io.turntabl.accramegastore.services.recommendation.RecommendationService;
import io.turntabl.accramegastore.services.ShoppingCart;
import io.turntabl.accramegastore.services.product.ProductService;
import io.turntabl.accramegastore.services.tax.TaxCalculator;

import java.util.*;

public class Application {

    private final ShoppingCart shoppingCart;
    private final RecommendationService recommendationService;
    private final ProductService productService;
    private final ComplementaryService complementaryService;

    // TODO: Implement fixed tax calculator.
    private final TaxCalculator taxCalculator = null;

    private static final String RETURN = "return";
    private static final String YES = "y";

    public Application() {
        productService = new ProductService("products.csv");
        complementaryService = new ComplementaryService("complementary_products.csv");
        shoppingCart = new ShoppingCart(productService);
        recommendationService = new RecommendationService(productService, complementaryService);
    }

    private void display() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("---------------\n");
        stringBuilder.append("Items in Store \n");
        stringBuilder.append("---------------\n");

        List<Product> products = productService.getProducts();
        stringBuilder.append(products.stream().map(Product::toString).reduce((a,b) -> a + "\n"  + b));
        System.out.println(stringBuilder);
    }

    private void add() {
        System.out.println("Enter the product id to add " +
                "(or type `return` to go back to the main menu): ");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        try {
            if (input.equals(RETURN)) return;
            shoppingCart.addProduct(input, 1);

            // TODO: recommend complementary item.

            System.out.printf("Item: %s successfully added. Enter \"y\" to add another product or anythings else to return to the main menu. \n", input);
            input = scanner.next();

            if (input.equals(YES)) {
                add();
            }
        } catch (UnknownProductException e) {
            System.out.print(e.getMessage() + ", ");
            add();

        } catch(UnavailableProductException e) {
            System.out.println("The product you've selected is unavailable. ");

            Optional<Product> similarProduct = recommendationService.getSimilarProduct(input);
            similarProduct.ifPresent(product -> System.out.printf("Here is an alternative product: %s \n", product));
            add();
        }
    }

    private void remove() {
        System.out.println("Enter the product id to remove " +
                "(or type `return` to go back to the main menu): ");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        try {
            if (input.equals(RETURN)) return;
            shoppingCart.removeProduct(input, 1);
        }
        catch (UnavailableProductException e) {
            System.out.println("The product you've selected is not in your basket. ");
            remove();
        }
    }

    private void checkout() {
        List<Item> items = shoppingCart.getItems();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("---------------\n");
        stringBuilder.append("Items in Basket\n");
        stringBuilder.append("---------------\n");

        stringBuilder.append(items.stream().map(Item::toString).reduce((a,b) -> a + "\n" + b));

        double total = shoppingCart.calculateTotal();

        DiscountService discountService = (List<Item> discountItems) -> {
            double discount = 0d;

            for (Item item : discountItems) {
                int itemsForFree = item.getQuantity() / 3;
                discount += itemsForFree * item.getPrice();
            }

            return discount;
        };
        double discount = discountService.calculateDiscount(items);

        // TODO Implement tax calculator.
        double tax = 0;

        // display cart summary
        stringBuilder.append("---------------\n");
        stringBuilder.append("Total: \t\t\t" + total + "\n");
        stringBuilder.append("Discount: \t\t" + discount + "\n");
        stringBuilder.append("Tax: \t\t\t" + tax + "\n");
        stringBuilder.append("Total to Pay: \t" + (total - discount + tax) + "\n");
        System.out.println(stringBuilder);

        // confirm checkout
        System.out.println("Enter \"y\" to confirm if you'd like to checkout or anything else to return to the main menu.");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        if (input.equals(YES)) {
            exit();
        }
    }

    public void exit() {
        System.out.println("Thanks for visiting the Accra Mega Store, Goodbye!");
        System.exit(0);
    }

    public static void main(String[] args) {
        System.out.println("Welcome to the Accra Mega Store!");
        Application application = new Application();

        Scanner scanner;
        Command command = null;

        do {
            System.out.println("Enter one of the following commands: " + Arrays.toString(Command.values()));
            scanner = new Scanner(System.in);
            try {
                command = Command.parse(scanner.next());

                switch (command) {
                    case DISPLAY:
                        application.display();
                        break;
                    case ADD:
                        application.add();
                        break;
                    case REMOVE:
                        application.remove();
                        break;
                    case CHECKOUT:
                        application.checkout();
                        break;
                }
            } catch (UnknownCommandException e) {
                System.out.println(e.getMessage());
            }
        } while(command != Command.EXIT);
    }

    private enum Command {
        ADD, REMOVE, DISPLAY, SHOW_CART, CHECKOUT, EXIT;

        public static Command parse(String value) {
            Optional<Command> commandFound = Arrays.stream(Command.values()).filter(command -> command.toString().equalsIgnoreCase(value)).findFirst();
            if(commandFound.isPresent()) return commandFound.get();
            throw new UnknownCommandException(
                    "Unknown command: " + value +
                            ". Valid values are: " + Arrays.toString(Command.values()));
        }
    }
}
