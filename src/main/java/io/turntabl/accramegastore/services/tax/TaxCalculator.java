package io.turntabl.accramegastore.services.tax;

import io.turntabl.accramegastore.services.Item;

import java.util.List;

public interface TaxCalculator {
    double calculateTax(List<Item> items);
}
