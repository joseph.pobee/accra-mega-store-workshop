package io.turntabl.accramegastore.services.discount;

import io.turntabl.accramegastore.services.Item;

import java.util.List;

@FunctionalInterface
public interface DiscountService {

    double calculateDiscount(List<Item> items);
}
